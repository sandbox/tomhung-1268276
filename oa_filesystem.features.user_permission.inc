<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function oa_filesystem_user_default_permissions() {
  $permissions = array();

  // Exported permission: Allow Reordering
  $permissions['Allow Reordering'] = array(
    'name' => 'Allow Reordering',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: create file content
  $permissions['create file content'] = array(
    'name' => 'create file content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: create folder content
  $permissions['create folder content'] = array(
    'name' => 'create folder content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: delete any file content
  $permissions['delete any file content'] = array(
    'name' => 'delete any file content',
    'roles' => array(),
  );

  // Exported permission: delete any folder content
  $permissions['delete any folder content'] = array(
    'name' => 'delete any folder content',
    'roles' => array(),
  );

  // Exported permission: delete own file content
  $permissions['delete own file content'] = array(
    'name' => 'delete own file content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: delete own folder content
  $permissions['delete own folder content'] = array(
    'name' => 'delete own folder content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: edit any file content
  $permissions['edit any file content'] = array(
    'name' => 'edit any file content',
    'roles' => array(),
  );

  // Exported permission: edit any folder content
  $permissions['edit any folder content'] = array(
    'name' => 'edit any folder content',
    'roles' => array(),
  );

  // Exported permission: edit own file content
  $permissions['edit own file content'] = array(
    'name' => 'edit own file content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  // Exported permission: edit own folder content
  $permissions['edit own folder content'] = array(
    'name' => 'edit own folder content',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'manager',
    ),
  );

  return $permissions;
}
