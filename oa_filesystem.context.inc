<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function oa_filesystem_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'oa_filesystem';
  $context->description = '';
  $context->tag = 'Open Atrium Filesystem';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'file' => 'file',
        'folder' => 'folder',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'oa_filesystem_files' => 'oa_filesystem_files',
        'oa_filesystem_folders' => 'oa_filesystem_folders',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(),
      'layout' => 'wide',
    ),
    'menu' => 'features',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Open Atrium Filesystem');
  $export['oa_filesystem'] = $context;

  return $export;
}
